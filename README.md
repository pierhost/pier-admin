# Pier Administration API 

This is the API for the Pier Administration service.
It is based on Node.js and Express.

To install it, please see the sections below. It's quite hard to test locally (i.e. not on a PierHost environment) as it relies on this envt so it's recommended to test it using Docker and updating the image on a pier host. Nevertheless, it's possible to test it, see [Local Testing](#local-testing)

You can test it with its compagnon project [pier-admin-ui](https://gitlab.com/pierhost/pier-admin-ui).
Or you can try the API using [Postman](https://www.postman.com/) by importing the collection in test/Test\ Pier\ Admin\ API.postman_collection.json
The API is password protected so you have to first call the login entrypoint. You will find the default password in src/persistence/login-db.ts
You must first set a new password with the POST /auth/password entrypoint.
Once done, you can then call the other services, e.g. GET on /services should display something like:
```
{
	"db":{"activated":true},
	"traefik":{"activated":true,"https":false,"challenge":"tls"},
	"adminer":{"activated":true},
	"bitwarden":{"activated":true},
	"gogs":{"activated":false},
	"mailu":{"activated":false},
	"nextcloud":{"activated":true},
	"pihole":{"activated":true},
	"cops":{"activated":true},
	"portainer":{"activated":true}
}
```
The open API specs is described in pier-admin-api.yml, it's documentation is currently published [here](https://studio-ws.apicur.io/sharing/4389dd83-58c7-4991-b44f-c937f65f7b8f).

### Work in progress
* Adding admin features: 
	* manage Wifi network (started)
	* ...

# Docker
After having installed docker (`sudo apt install docker` or `docker.io` or `docker-ce` depending on your distro), apply these 2 commands to ensure you'll have access to docker:

	$ sudo groupadd docker
  	$ sudo usermod -aG docker $USER

It's very lilely you'll need to restart a terminal (or even reboot) so that this is taken into account. You can check it's done by simply running `$ id` 
which should list docker as being part of your groups, e.g. `uid=1000(youruser) gid=1000(yourgroup) groups=1000(yourgroup),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),114(lpadmin),134(sambashare),135(docker)`

Now, test you access to docker eg with `$ docker ps`

## Install buildx
If you want to build the docker versions of this project on a linux distro which is not Raspbian, buildx needs to be installed along with your docker install.

Buildx comes bundled in recent versions of docker (installed through a .deb package). If `docker buildx` fails, saying buildx is not a docker command command, you will need to install it manually (although you are encouraged to install a recent docker version!).

**WARNING** the snap version of Docker on Ubuntu is not using ~/.docker directory, look in ~/.snap/...

Look for the latest version of buildx here: https://github.com/docker/buildx/releases/latest and set BUILDX_VERSION with this version e.g. `BUILDX_VERSION=0.7.1`.

The commands to install buildx look like this:

	$ buildx="$HOME/.docker/cli-plugins/docker-buildx"
	$ mkdir -p $(dirname $buildx)
	$ wget -O $buildx https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64
	$ chmod a+x $buildx

Check that "experimental" is set to "enabled" in ~/.docker/config.json

Now, try to run `docker buildx`, this should **NOT** display *docker: 'buildx' is not a docker command.*

## build.sh script
The build.sh script build a docker image of the project, taking care of the multiarch setup (see below).

__WARNING: don't use the script before looking at following § about multiarch__

    $ ./build.sh -h
	usage: ./build.sh [-r (dockerhub|gitlab)] [-d devdir] [-h] [-p] [-t tag] [-n|N]
		-h shows this help and exits
		-p pushes the image to docker hub after building. You must be logged to docker hub first
		-r docker registry where the image is pushed. default = dockerhub (useful only for -p)
		-d devt dir. default = .
		-t tag used to build and push the image, default tag is computed based on the current branch, currently: develop-latest.
		-n no-cache when building
		-N prunes buildx cache totally

##	 Setup of multiarch with buildx
The `build.sh` script does create the needed envt but you must check *before* that the prerequistes are ok on your host.

To enable buildx to use qemu to build for arm, if
1. you're building on a system where you *didn't* install qemu-user-static:i386 which is needed by the pierhost core, see [dependencies section](https://gitlab.com/pierhost/core#dependencies))
2. and you are using a recent system, 
then it should be enough to run the following command (as described on the [docker buildx page](https://docs.docker.com/buildx/working-with-buildx/))

    $ docker run --privileged --rm tonistiigi/binfmt --install all

If the script output does not show `installing: arm OK` but shows instead `installing: arm qemu-arm already registered`, it means you probably already have quemu-user-static:i386 installed. If you don't need it, remove it (`sudo apt remove quemu-user-static:i386`) and retry.

If the build.sh script fails in the first npm command run by your Dockerfile, first check the output of `docker build ls`: in the supported platform list, you should find linux/arm/v7. Same as above, check for the package quemu-user-static:i386, remove it and retry the above docker command.

If you want to keep the quemu-user-static:i386 package because you are also building the core project, please check [this page](https://medium.com/@artur.klauser/building-multi-architecture-docker-images-with-buildx-27d80f7e2408) which explains very clearly more prerequisites and how to install them. 

## Build the image
Just run (-p is for pushing to a registry)

    ./build.sh [-p]

See other options with `./build.sh -h` 
Obviously, to be able to push to mypier, you must
 1) be part of the mypier organization on Docker Hub
 2) have firt run `docker login` 

 ## environment
 You can set the following variables, but the default values set in start.sh should be working fine:

* `JWT_TIMEOUT`: in minutes, defaults to 60mn
* `BASE_URL`: defaults to "/"
* `CORS_DOMAINS`: defaults to nothing as UI and API should run on the same host using the same IP or domain. Can be set for instance to http://localhost:9090 is the API should be called from a test UI running on this URL.
* `WEBSOCKET_URL`: Url to the pier websocket, something like 'ws://pierhost:8088'

## Reverse proxying with Apache
As an example, here is the ProxyPass config I'm using for reverse proxying the Pier Admin UI and API.
First

    define PierInternalURI http://<the IP of your RPi on the LAN>`

Then, assuming you want to access the UI on /admin and you kept the standard parameters for the API, add the following lines in the VHost definition:
```
ProxyPass "/" "${PierInternalURI}/"
ProxyPassReverse "/" "${PierInternalURI}/"
```

# Local testing
If you want to test pier-admin + pier-admin-ui locally (docker or not, semi fake test envt), please follow the instructions on the [pier-admin wiki](https://gitlab.com/pierhost/pier-admin/-/wikis/home)
