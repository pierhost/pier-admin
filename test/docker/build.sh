#!/bin/bash

IMAGE=pier-adm-testenv

GITLAB_REGISTRY=registry.gitlab.com/pierhost
DOCKERHUB_REGISTRY=mypier
DOCKER_REGISTRY_TYPE=dockerhub
#PLATFORMS='linux/arm/v7,linux/arm64,linux/amd64'
PLATFORMS='linux/arm/v7'
BASEDIR=$(cd $(dirname $0)/.. && pwd)
#TAG=$(git branch --show-current)-latest
TAG=arm-v7-latest

function usage() {
	echo "usage: $0 [-r (dockerhub|gitlab)] [-d BASEDIR] [-h] [-p] [-t tag] [-n|N]"
	echo "-h shows this help and exits"
	echo '-p pushes the image to docker hub after building. You must be logged to docker hub first'
	echo "-r docker registry where the image is pushed. default = ${DOCKER_REGISTRY_TYPE} (useful only for -p)"
	echo "-d base dir. default = ${BASEDIR}"
	echo "-t tag used to build and push the image, default tag is computed based on the current branch, currently: ${TAG}."
	echo "-n no-cache when building"
	echo "-N prunes buildx cache totally"
	exit 1
}

while getopts ":hd:r:t:nNp" flag
do
	case "${flag}" in
		h) usage;;
		d) BASEDIR="${OPTARG}";;
		r) DOCKER_REGISTRY_TYPE="${OPTARG}";;
		t) TAG="${OPTARG}";;
		n) NOCACHE='--no-cache';;
		N) docker buildx prune -a -f;;
		p) PUSH='--push';;
		*) usage;;
	esac
done

case "${DOCKER_REGISTRY_TYPE}" in
  'gitlab') DOCKER_REGISTRY="${GITLAB_REGISTRY}";;
  'dockerhub') DOCKER_REGISTRY="${DOCKERHUB_REGISTRY}";;
  *) echo "Unknown docker registry:" ${DOCKER_REGISTRY_TYPE};
     usage;;
esac

# create docker context
BUILDER='dwbx_builder'
EXISTS=$(docker buildx ls | grep -e "^${BUILDER}")
if [ -z "${EXISTS}" ]; then
	docker buildx create --name ${BUILDER}
	docker buildx use ${BUILDER}
	docker buildx inspect --bootstrap
else
	docker buildx use ${BUILDER}
fi

# get docker dir
DOCKER_DIR=$(cd $(dirname $0) && pwd)
# install emulators for multiarch
docker run --privileged --rm tonistiigi/binfmt --install all
# build and push 
set -x
docker buildx build --platform ${PLATFORMS} ${NOCACHE} --tag ${DOCKER_REGISTRY}/${IMAGE}:${TAG} ${PUSH} -f ${DOCKER_DIR}/Dockerfile ${BASEDIR}
