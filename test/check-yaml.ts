// for testing purpose

import { ConfigDBManager } from '@/storage/config-db';

async function main() {
	const psl = ConfigDBManager.fetchData();
	psl.delete("db");
	const ps = psl.get("traefik");
	if (!ps) return;
	ps.https = true;
	psl.set("traefik", ps);
	ConfigDBManager.persistData(psl);
}

main();