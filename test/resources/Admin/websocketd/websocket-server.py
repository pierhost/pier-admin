#!/usr/bin/python3
# File: websocketd-server.py
# websocket server for executing "bare metal" commands in pier admin
# Author: Olivier LEVILLAIN
# January 03, 2021
from sys import stdout, stdin
import threading
import json
import subprocess
import logging
import traceback
import jwt 
import pprint

websocket_dir="."

def usage(): 
    print ("$ cd %s/websocketd && ./websocketd --port=8088  ./websocketd-server.py" % websocket_dir)

class Config:
    def __init__(self, cfg_file = websocket_dir + '/config.json'):
        with open(cfg_file) as f:
            self.__dict__ = json.load(f)
        if self.__dict__.get('jwt_key_file') == None: raise Exception("No JWT key file in config.json")
        with open(self.jwt_key_file) as f:
            self.jwt_key = f.read()
        if self.__dict__.get('log_level') == None: self.log_level = logging.WARNING
        elif self.log_level == 'debug': self.log_level = logging.DEBUG
        elif self.log_level == 'info': self.log_level = logging.INFO
        elif self.log_level == 'warning': self.log_level = logging.WARNING
        elif self.log_level == 'error': self.log_level = logging.ERROR
        elif self.log_level == 'critical': self.log_level = logging.CRITICAL
        else: assert False, "log level %s unknown" % self.log_level
        logger = logging.getLogger()
        logger.setLevel(self.log_level)
        if self.__dict__.get('log_file') == None: self.log_file = None
        ch = logging.FileHandler(self.log_file) if self.log_file else logging.StreamHandler()
        ch.setLevel(self.log_level)
        ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        logger.addHandler(ch)
        if self.__dict__.get('authorized_commands_file') == None: self.authorized_commands = []
        else: 
            with open(self.authorized_commands_file) as f:
                self.authorized_commands = json.load(f)
        # logging.info('authorized commands:'+pprint.pformat(self.authorized_commands))

class CmdResult:
    def __init__(self, status = -1, out = ''):
        self.status = status if status else 0
        self.out = str(out).splitlines()

    def toJson(self) -> str:
        return json.dumps(self.__dict__, separators=(',', ':'))

    def reply(self):
        res=self.toJson()
        logging.info("reply: " + res)
        print(res)
        stdout.flush()


def send_error(err = None, status = -1, message = None):
    if err:
        logging.error(err)
        logging.error(traceback.format_exc())
    if not message:
        if err:
           message = pprint.pformat(err)
        else:
           message = 'Unknown Error' 
    CmdResult(status, message).reply()

class Process:
    def __init__(self, cmd):
        try:
            if (cmd.sync): self.sync_exec(cmd)
            else: self.async_exec(cmd)
        except Exception as err:
            send_error(err)

    def sync_exec(self, cmd) -> CmdResult:
        try:
            proc = subprocess.run(cmd.command(), input=cmd.input, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, timeout=cmd.timeout, cwd=cmd.cwd, encoding='utf-8')
            cmd=CmdResult(status=proc.returncode, out=proc.stdout)
            cmd.reply()
        except subprocess.TimeoutExpired as err:
            send_error(err, -100, 'Time Out Expired')

    def async_exec(self, cmd):
        proc = subprocess.Popen(cmd.command(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE, cwd=cmd.cwd, encoding='utf-8')
        # TODO: manage timeout in command and fix input
        if (cmd.input): proc.stdin.write(cmd.input.strip() + '\n')
        for line in proc.stdout:
            # just send one line string back to caller
            logging.info("one line: " + line)
            print('"', line.rstrip(), '"')
            stdout.flush()
        cmd=CmdResult(status=proc.returncode)
        cmd.reply()

class Command:
    """Class for defining a command to be run."""

    def __init__(self, line):
        self.cmd = None
        self.token = None
        self.args = []
        self.sync = True
        self.input = None
        self.timeout = 120  # default timeout = 2m
        self.cwd = None
        cmd = json.loads(line)
        for key, val in cmd.items():
            self.__dict__[key] = val
        logging.info("received: " + str(json.dumps(self.__dict__, separators=(',', ':'))))
        self.check_cmd()

    def check_cmd(self):
        # TODO: check also CGI variables like HTTP_ORIGIN (equiv CORS)
        assert self.cmd != None, "Command not defined!"
        assert self.token != None, "No token found in command"
        try:
            payload = jwt.decode(self.token, config.jwt_key, algorithms=["HS256"])
            #logging.debug("jwt payload:" + pprint.pformat(payload))
        except jwt.exceptions.InvalidTokenError as err:
            send_error(err, -150, 'Invalid Token')
            assert False, "Invalid token"
        assert payload.get('payload') and payload.get('payload') == config.jwt_key, "Wrong payload in token"
        assert self.cmd in config.authorized_commands, 'trying to execute cmd ' + self.cmd + ' in websocketd which is not allowed'

    def command(self):
        return [self.cmd] + self.args

def serve():
    while True:
        # receive cmds from the pier API
        line = stdin.readline().strip()
        if not line: break
        try:
            cmd = Command(line)
            proc = Process(cmd)
        except Exception as err:
            send_error(err)

# load config
config = Config()
# starts N threads to receive and execute the cmds
N=2
logging.info ("starting websocketd server...")
for i in range(N):
    t = threading.Thread(target=serve)
    t.start()
    # suspend main thread until worker threads are done
    t.join()

