#!/bin/bash


IMAGE=pier-admin
GITLAB_REGISTRY=registry.gitlab.com/pierhost
DOCKERHUB_REGISTRY=mypier
DOCKER_REGISTRY_TYPE=dockerhub
#PLATFORMS='linux/arm/v7,linux/arm64,linux/amd64'
PLATFORMS='linux/arm/v7'
DEVDIR=.

function slugify() {
	echo "$1" | iconv -t ascii//TRANSLIT | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr A-Z a-z
}

function git_current_branch() {
  # show-current added in recent version of git
	git branch --show-current 2>/dev/null || git branch | grep '^*' | sed -e 's/* //'
}

function usage() {
	echo "usage: $0 [-r (dockerhub|gitlab)] [-d devdir] [-h] [-p] [-t tag] [-n|N]"
	echo "-h shows this help and exits"
	echo '-p pushes the image to docker hub after building. You must be logged to docker hub first'
	echo "-r docker registry where the image is pushed. default = ${DOCKER_REGISTRY_TYPE} (useful only for -p)"
	echo "-d devt dir. default = ${DEVDIR}"
	echo "-t tag used to build and push the image, default tag is computed based on the current branch, currently: ${TAG}."
	echo "-n no-cache when building"
	echo "-N prunes buildx cache totally"
	exit 1
}

while getopts ":hd:r:t:nNp" flag
do
	case "${flag}" in
		h) usage;;
		d) DEVDIR="${OPTARG}";;
		r) DOCKER_REGISTRY_TYPE="${OPTARG}";;
		t) TAG="${OPTARG}";;
		n) NOCACHE='--no-cache';;
		N) docker buildx prune -a -f;;
		p) PUSH='--push';;
		*) usage;;
	esac
done

TAG=$(slugify $(git_current_branch))
case "${DOCKER_REGISTRY_TYPE}" in
  'gitlab') DOCKER_REGISTRY="${GITLAB_REGISTRY}";;
  'dockerhub') DOCKER_REGISTRY="${DOCKERHUB_REGISTRY}";;
  *) echo "Unknown docker registry:" ${DOCKER_REGISTRY_TYPE};
     usage;;
esac

# create docker context
BUILDER='pierhost_builder'
EXISTS=$(docker buildx ls | grep -e "^${BUILDER}")
if [ -z "${EXISTS}" ]; then
	docker buildx create --name ${BUILDER}
	docker buildx use ${BUILDER}
	docker buildx inspect --bootstrap
else
	docker buildx use ${BUILDER}
fi
# install emulators for multiplatform
docker run --privileged --rm tonistiigi/binfmt --install $PLATFORMS
set -x
# build and push 
docker buildx build --platform ${PLATFORMS} ${NOCACHE} --tag ${DOCKER_REGISTRY}/${IMAGE}:${TAG} ${PUSH} ${DEVDIR}
