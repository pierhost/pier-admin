
// Import the oak utils for app and router. Oak works similar than Express in Node, we are using the version 4.0.0 of oak
import express from 'express';
import cookieParser from 'cookie-parser';
import { APP_PORT } from './config';
import routers from './routes';
import { HandlerUtils } from "./handlers/handler-utils";

// Start instances of app and router
const app = express();

app
	// tells express to use a json parser
	.use(express.json())
	// also add cookies
	.use(cookieParser())
	// manage CORS
	.use(HandlerUtils.manageHeaders)
	// now route requests using the routers
	routers.forEach(router => app.use(router));


// Start the app on the provided port
console.log(`Listen on port ${APP_PORT}`);
app.listen(APP_PORT);
