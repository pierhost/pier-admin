import { WireguardDB } from "../storage/wg-db";
import { WireguardPeer, WireguardPeerStr, WireguardPeerFull } from "../models/wireguard-peer";

const wGdB = new WireguardDB();
export class WireGuardEntryPoint {
	static async restartWireguard(): Promise<void> {
		await wGdB.systemctl("restart", "wg-quick@wg0");
	}
	static async createPeer() : Promise<WireguardPeerStr> {
		const userId = wGdB.getNewUserId();
		// Creating client's Wireguard key pair for user userId
		const clientPrivateKey = await wGdB.runCmd ("/usr/bin/wg", ["genkey"]); // the keys are base64 encoded, padded with =
		const clientPublicKey = await wGdB.runCmd("/usr/bin/wg", ["pubkey"], { input: clientPrivateKey });
		// Update the server's conf
		const peer = wGdB.createPeer(userId, clientPrivateKey, clientPublicKey);
		await WireGuardEntryPoint.restartWireguard();
		return peer;
	}
	static listPeers() : WireguardPeerFull[] {
		return wGdB.listPeers();
	}
	static async removePeer(id: number): Promise<void> {
		wGdB.removePeer(id);
		await WireGuardEntryPoint.restartWireguard();
	}
	static async updatePeer(id: number, peer: WireguardPeer): Promise<void> {
		wGdB.replacePeer(id, peer);
		await WireGuardEntryPoint.restartWireguard();
	}
	static async addExistingPeer(peer: WireguardPeer): Promise<number> {
		const id = wGdB.addExistingPeer(peer);
		await WireGuardEntryPoint.restartWireguard();
		return id;
	}
}