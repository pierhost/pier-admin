import Docker from "dockerode";
import { Container } from 'dockerode';
import { Stream } from 'stream';
import { sleep } from "../utils";
const docker = new Docker({ socketPath: '/var/run/docker.sock' });

const LOG_MAX_SIZE = 100;
export class PierContainer {
    id: string;
    container: Container;
    stream: Stream | undefined;
    stdout: string[] = [];
    stderr: string[] = [];
    constructor(id: string) {
        this.id = id;
        this.container = docker.getContainer(id);
    }
    private async getStream() {
        if (! this.stream) {
            this.stream = await this.container.logs({
                follow: true,
                stdout: true,
                stderr: true,
                tail: LOG_MAX_SIZE
            }) as Stream;
            this.stream.on('data', (info: Buffer) => this.addLog(this.stdout, info));
            this.stream.on('error', (err: Buffer) => this.addLog(this.stderr, err));
            await sleep(2000); // wait for 2s to be sure we got the first lines.
        }
    }
    private addLog(log: string[], line: Buffer) {
        const str =line.toString("utf-8").slice(8);  // don't know why the 8 first chars are crap...
//        console.debug("logging", str);
        log.push(str);
        while (log.length > LOG_MAX_SIZE) log.shift();
    }
    async getLogs(): Promise<{ stdout:string[]; stderr: string[]}> { 
        await this.getStream();
        return { stdout: this.stdout, stderr: this.stderr};
    }
}

class DockerEntrypoint {
    private containerCache = new Map<string, PierContainer>();
    getContainer(id: string): PierContainer {
        if (this.containerCache.has(id)) return this.containerCache.get(id) as PierContainer;
        // not found, create it
        const pc = new PierContainer(id);
        // register container in cache
        this.containerCache.set(id, pc);
        // flush container from cache after 1mn. Next call will recharge it if needed and if it still exists
        setTimeout(() => this.containerCache.delete(id), 60000);
        return pc; 
    }
    async listContainers() {
        return await docker.listContainers();
    }
}

export const dockerEntryPoint = new DockerEntrypoint();