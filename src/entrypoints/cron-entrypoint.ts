import { WebSocketClient } from "../storage/websocket-client";

function escapeRegExp(s: string){
	// $& means the whole matched string
	return s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}
const wsc = new WebSocketClient();
/**
 * Class implementing the Pier services PI
 * Uses the ConfigDBManager for fetching/storing the config.yml data
 */
export class CronEntryPoint {
	public static async manageCronjob(command: string, args: string[] = [], every: number|undefined, remove: boolean | undefined): Promise<string> {
		let crontab: string;
		if (!remove && !every) return Promise.reject("Cronjob 'every' parameter missing");
		try {
			crontab = await wsc.runCmd("/usr/bin/crontab", ["-l"]);
			crontab = crontab.trim() + "\n";
		} catch (err) {
			if (err.out && err.out[0].startsWith("no crontab for")) crontab = "";
			else throw err;
		}
		const sargs = (args.length > 0 ? '"' + args.map(arg => arg.replace('"', '\\"')).join('" "') + '"' : "");
		let newtab: string;
		if (remove) {
			const r = "^" + 
				(every ? escapeRegExp(`*/${every} * * * * `) : ".*\\* ") +
				command + " " + escapeRegExp(sargs) + ".*$";
			console.log("regex:", r);
			console.log("match:", crontab.match(new RegExp(r, "m")))
			newtab = crontab.replace(new RegExp(r, "m"), "");
		} else {
			const job = `*/${every} * * * * ${command} ${sargs}`;
			newtab = crontab + job + "\n";
		}
		console.log("new crontab:\n", newtab);
		return await wsc.runCmd("/usr/bin/crontab", ["-"], { input: newtab })
	}
	public static async getCrontab(): Promise<string[]> {
		try {
			const crontab = await wsc.runCmd("/usr/bin/crontab", ["-l"]);
			// console.log("crontab:\n", crontab);
			return crontab.split("\n").filter(line => !line.startsWith("#") && line !== "");
		} catch (err) {
			console.log("Error:", err);
			if (err.out && err.out[0].startsWith("no crontab for")) return [];
			else throw err;
		}
	}
}
