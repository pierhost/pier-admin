import { NetworkDBManager } from "../storage/network-db";
import { AdminDBManager } from "../storage/admin-db";
import { DhcpNetworkInterface, WifiNetworkInfo } from "../models/network-interface";
import { Response } from "express";
import { HOTSPOT_CMD } from "../config";
export type confKind = "admin" | "critical" | "user";
const netDb = new NetworkDBManager();
const admDb = new AdminDBManager();

export type DomainAndContact = {domain:string; contact:string};
/**
 * Class implementing the Pier services PI
 * Uses the ConfigDBManager for fetching/storing the config.yml data
 */
export class NetworkEntryPoint {
	public static getDomainAndContact(): DomainAndContact {
		return { domain: admDb.getEnvVal("DOMAIN"), contact: admDb.getEnvVal("CERT_EMAIL") };
	}
	public static async updateDomainContact(dnc: DomainAndContact): Promise<void> {
		admDb.setEnvVal("DOMAIN", dnc.domain);
		admDb.setEnvVal("CERT_EMAIL", dnc.contact);
	}
	public static async changeNetworkInterface(dhcpInt: DhcpNetworkInterface): Promise<void> {
		return await netDb.changeNetworkInterface(dhcpInt);
	}
	public static async getDefaultNetworkInterface(): Promise<DhcpNetworkInterface> {
        // returns "default via {routerIP} dev {interface} proto protocol src {deviceIP} rest;
        const iproute = await netDb.runCmd("/usr/sbin/ip", ["route", "list", "default"]);
        const routeData = iproute.split(' ');
		let res: DhcpNetworkInterface = { name: "?", routerIpAddress: "?", ipAddress: "?", dnsIpAddress: "?" };
		let i = 0;
		while (i<routeData.length) {
			if (routeData[i] === "via") res.routerIpAddress = res.dnsIpAddress = routeData[++i];
			if (routeData[i] === "dev") res.name = routeData[++i];
			if (routeData[i] === "src") res.ipAddress = routeData[++i];
			i++;
		}
        return res;
    }
	public static getInterfacesIp(): Record<string, string[]> {
		return netDb.getMyIPs(["eth0", "wlan0"]);
	}
	public static async hotspot(action: string, stream: Response): Promise<string> {
		return netDb.runCmd(HOTSPOT_CMD, [action], stream);
	}
	public static async ethernetConnected(): Promise<boolean> {
		// File not mounted in container => must use a command to read it...
		// Using tail instead of cat because already used by other commans
		return (await netDb.runCmd("/usr/bin/tail", [ "/sys/class/net/eth0/carrier" ])) === "1"
	}
	public static async scanWifi(): Promise<WifiNetworkInfo[]> {
		// make sure wpa supplicant is running before scanning
		await netDb.systemctl("restart", "wpa_supplicant"); // TODO: try/catch?
		return await netDb.scanWifi();
	}
	public static async addWifiNetwork(network: WifiNetworkInfo, stream: Response) {
		await netDb.addWpaSupplicantNetwork([network]);
		await netDb.systemctl("restart", "wpa_supplicant"); // TODO: try/catch?
		return this.hotspot("stop", stream);
	}
}
