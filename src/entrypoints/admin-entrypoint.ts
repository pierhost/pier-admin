import { UPDATE_SYSTEM_CMD, REGENSRV_CMD, SERVICES_DEFAULT_BRANCH, SERVICES_DIR,
	ADMIN_CONFIG_PATH, CRITICAL_CONFIG_PATH, CONFIG_PATH, SAMPLE_CONFIG_PATH } from '../config';
import { AdminDBManager } from "../storage/admin-db";
import { AsyncCmdResult, CmdResult } from "../storage/websocket-client";
import { Response } from "express";
import { PierServicesObject } from "../models/pier-service";
import { ConfigDBManager } from "../storage/config-reader";

export type confKind = "admin" | "critical" | "user";
function escapeRegExp(s: string){
	// $& means the whole matched string
	return s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

const PIER_SERVICES_DIR = SERVICES_DIR + "/pier-services";
const admDb = new AdminDBManager();
/**
 * Class implementing the Pier services PI
 * Uses the ConfigDBManager for fetching/storing the config.yml data
 */
export class AdminEntryPoint {
	public static asyncUpdateServices(kind: confKind): AsyncCmdResult {
		return admDb.asyncRunCmd(REGENSRV_CMD, [kind], {timeout:200});
	}
	public static async streamUpdateServices(kind: confKind, stream: Response): Promise<string> {
		return admDb.runCmd(REGENSRV_CMD, [kind], {timeout:200}, stream);
	}
	public static async streamRebootServer(stream: Response): Promise<string> {
		return admDb.runCmd("/usr/sbin/reboot", ["-f", "--no-wall"], stream);
	}
	public static async streamUpdateSystem(stream: Response): Promise<string> {
		return admDb.runCmd(UPDATE_SYSTEM_CMD, [], {timeout:240}, stream);
	}
	public static getAllPasswords(): Record<string, string> {
		return admDb.getPasswords();
	}
    public static getConfigMgr(kind: confKind): ConfigDBManager {
        return new ConfigDBManager(kind == "admin" ? ADMIN_CONFIG_PATH : kind == "critical" ? CRITICAL_CONFIG_PATH : CONFIG_PATH);
	}
    public static getConfig(kind: confKind): PierServicesObject {
        return this.getConfigMgr(kind).fetchData();
    }
    public static putConfig(svcs: PierServicesObject, kind: confKind): void {
        this.getConfigMgr(kind).persistData(svcs);
    }
	public static async updateServicesFromRepo(branch: string | undefined = undefined): Promise<void> {
		let curBranch = null;
		curBranch = await admDb.runCmd("/usr/bin/git", ["symbolic-ref", "--short", "-q", "HEAD"], { cwd: PIER_SERVICES_DIR })
				.catch(e => curBranch=null);
		console.debug("pier services current branch is", curBranch);
		// use default branch if no current branch and none requested
		if (!branch && !curBranch) branch = SERVICES_DEFAULT_BRANCH;
		// checkout the requested branch if != current branch
		if (branch && curBranch !== branch)
			await admDb.runCmd("/usr/bin/git", ["checkout", branch], { cwd: PIER_SERVICES_DIR });
		try {
			await admDb.runCmd("/usr/bin/git", ["pull"], { cwd: PIER_SERVICES_DIR });
		} catch (err) {
			console.error(err);
		}
	}
	public static async getDeltaUserConfig(): Promise<PierServicesObject> {
		try {
			await AdminEntryPoint.updateServicesFromRepo();
			const res: PierServicesObject = {};
			const sample = new ConfigDBManager(SAMPLE_CONFIG_PATH).fetchData();
			const user = AdminEntryPoint.getConfigMgr("user").fetchData();
			const userKeys = Object.keys(user);
			Object.keys(sample).filter(key => !userKeys.includes(key)).forEach(key => {
				res[key] = sample[key];
				res[key].activated = false;
			});
			console.debug("delta between", sample, "and", user, "gives", res);
			return res;
		} catch (err) {
			console.error("get delta user config error:", err);
			throw err;
		}
	}
	public static async manageCronjob(command: string, args: string[] = [], every: number|undefined, remove: boolean | undefined): Promise<string> {
		let crontab: string;
		if (!remove && !every) return Promise.reject("Cronjob 'every' parameter missing");
		try {
			crontab = await admDb.runCmd("/usr/bin/crontab", ["-l"]);
			crontab = crontab.trim() + "\n";
		} catch (err) {
			if (err.out && err.out[0].startsWith("no crontab for")) crontab = "";
			else throw err;
		}
		const sargs = (args.length > 0 ? '"' + args.map(arg => arg.replace('"', '\\"')).join('" "') + '"' : "");
		let newtab: string;
		if (remove) {
			const r = "^" + 
				(every ? escapeRegExp(`*/${every} * * * * `) : ".*\\* ") +
				command + " " + escapeRegExp(sargs) + ".*$";
			console.log("regex:", r);
			console.log("match:", crontab.match(new RegExp(r, "m")))
			newtab = crontab.replace(new RegExp(r, "m"), "");
		} else {
			const job = `*/${every} * * * * ${command} ${sargs}`;
			newtab = crontab + job + "\n";
		}
		console.log("new crontab:\n", newtab);
		return await admDb.runCmd("/usr/bin/crontab", ["-"], { input: newtab })
	}
	public static async getCrontab(): Promise<string[]> {
		try {
			const crontab = await admDb.runCmd("/usr/bin/crontab", ["-l"]);
			// console.log("crontab:\n", crontab);
			return crontab.split("\n").filter(line => !line.startsWith("#") && line !== "");
		} catch (err) {
			console.log("Error:", err);
			if (err.out && err.out[0].startsWith("no crontab for")) return [];
			else throw err;
		}
	}
	public static async getWsdLogs(nblines = 100): Promise<string[]> {
		try {
			return (await admDb.getWsdLogs(nblines)).split("\n");
		} catch (err) {
			console.log("get WSD Logs Error:", err);
			throw err;
		}
	}
	public static getCmdResults(cmdId: number): CmdResult {
		try {
			const res = admDb.asyncGetResults(cmdId);
			console.log("get res:\n", cmdId, res);
			return res;
		} catch (err) {
			console.log("get res Error:", err);
			throw err;
		}
	}
}
