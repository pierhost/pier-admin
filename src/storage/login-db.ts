import * as crypto from "crypto";
import { LOGIN_PATH } from "../config";
import { existsSync, readFileSync, writeFileSync} from 'fs';
const defaultPassword = 'Pi3r4dmin#Def4ultP4ssw0rd';
const specChar = "!\"#\\$%&'\\(\\)\\*\\+,-\\.\\/:;<=>\\?@[\\]\\^_`\\{\\|}~";
const pwdPattern = new RegExp(
  "^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[" +
  specChar +
  "])[a-zA-Z0-9" +
  specChar +
  "]{8,}$");

export class LoginDB {
	private static hashPwd(pwd: string, salt?: string): string {
		if (!salt) salt = crypto.randomBytes(16).toString('base64');
		const hash = crypto.createHmac('sha512', salt).update(pwd).digest("base64");
		return salt + "$" + hash;
	}
	private static storePwd(pwd: string) {
		const hash = LoginDB.hashPwd(pwd);
		console.log(hash);
		writeFileSync(LOGIN_PATH, hash);
	}
	static checkPwdPolicy(pwd: string): boolean {
		const match = pwd.match(pwdPattern);
		return pwd.length >= 12 && (match? true : false);
	}
	static changePwd(oldPassword: string, newPassword: string): void {
		//console.debug("change pwd", oldPassword, "to", newPassword);
		if (LoginDB.checkPwdPolicy(newPassword)) {
			if (LoginDB.checkPwd(oldPassword)) {
				LoginDB.storePwd(newPassword);
			} else throw Error('Wrong old password');
		} else throw Error('Password not matching policy');
	}
	static checkPwd(password: string): boolean {
		const stored: string = readFileSync(LOGIN_PATH, {encoding:'utf8'});
		const passwordFields = stored.split('$');
		const salt = passwordFields[0];
		return stored == LoginDB.hashPwd(password, salt);
	}
	static firstPwd(password: string): void {
		LoginDB.changePwd(defaultPassword, password);
	}
	// create the password file with default password if it doesn't exist
	private static inited: boolean = LoginDB.initPwd();
	private static initPwd():boolean {
		if (!existsSync(LOGIN_PATH)) LoginDB.storePwd(defaultPassword);
		return true;
	}
	static async mustChangePwd(): Promise<boolean> {
		return LoginDB.checkPwd(defaultPassword);
	}
}
