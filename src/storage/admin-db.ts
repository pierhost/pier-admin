import { readFileSync, writeFileSync, readdirSync } from "fs";
import { ENV_PATH, ISECRETS_DIR } from "../config";
import { WebSocketClient } from "./websocket-client";
export class AdminDBManager extends WebSocketClient {

    getRegexp(envVar: string) : RegExp {
        return new RegExp("^([\\t ]*" + envVar + "=)\"?([^#\"\n\r]*)\"?(.*)$", "m");
    }
    getEnvVal(envVar: string): string {
        const env = readFileSync(ENV_PATH, {encoding:'utf8'});
        const m = env.match(this.getRegexp(envVar));
        console.log("match=", m);
        if (!m) { // not found
            console.log("get env var ", envVar, "not found");
            return "";
        } else if (m.length > 4) // several matching lines
            throw Error('malformed environment file');
        else { 
            console.log(`getenv ${envVar} = ${m[2]}`);
            return m[2].trim();
        }
    }
    setEnvVal(envVar: string, val: string): void {
        const env = readFileSync(ENV_PATH, {encoding:'utf8'});
        const re = this.getRegexp(envVar);
        const m = env.match(re);
        if (!m) { // not found, add a new var
            writeFileSync(ENV_PATH, `\n${envVar}=${val}`, { flag: "a" })
        } else {
            const oldVal = m[2]; 
            if (oldVal !== val) {
                const newEnv = env.replace(re, "$1" + val + " $3");
                console.log("setenv", envVar, val, newEnv);
                writeFileSync(ENV_PATH, newEnv);
            } else {
                console.log("setenv", envVar, val, "ignored, same value.");
            }
        }
    }
    getPasswords(): Record<string, string> {
        const filteredPwds = [
// needed for adminer            "DATABASE_ADMIN_PASSWORD",
            "NEXTCLOUD_DB_PASS",
            "WEBSOCKETD_KEY",
            "pwd"
        ];
        const res: Record<string, string> = {};
        readdirSync(ISECRETS_DIR)
            .filter((file: string) => !filteredPwds.includes(file))
            .forEach ((file: string) => {
                res[file] = readFileSync(ISECRETS_DIR+ "/" +file, "utf8").trim();
            });
        console.log("get passwords:", res);
        return res;
    }
}