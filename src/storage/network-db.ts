import { readFileSync, writeFileSync } from "fs";
import { DHCPCD_CONF_PATH, WPA_SUPPLICANT_PATH } from "../config";
import { AsyncCmdResult, WebSocketClient } from "./websocket-client";
import { DhcpNetworkInterface, WifiNetworkInfo } from "../models/network-interface";
import { networkInterfaces } from "os";

export class NetworkDBManager extends WebSocketClient {
    // changes interface <int> IP addresses in (mounted) dhcpcd.conf and restarts dhcp
    async changeNetworkInterface(dhcpInt: DhcpNetworkInterface): Promise<void> {
        const dhcpcdConf = readFileSync(DHCPCD_CONF_PATH, "utf-8").split('\n');
        const resConf: string[] = [];
        let insideWantedInterface = false;
        // keep in resConf all lines except lines inside the wanted interface
        dhcpcdConf.forEach((line) =>{
            line = line.trimLeft();
            if (line.startsWith('#')) resConf.push(line);
            else if (!line.startsWith("interface") &&
                !line.startsWith("static domain_name_servers") &&
                !line.startsWith("static ip_address") &&
                !line.startsWith("static routers")) resConf.push(line)
            else if (line.startsWith(`interface ${dhcpInt.name}`)) insideWantedInterface = true;
            else if (line.startsWith("interface")) {
                insideWantedInterface = false;
                resConf.push(line);
            } else if (!insideWantedInterface && (
                line.startsWith("static domain_name_servers") ||
                line.startsWith("static ip_address") ||
                line.startsWith("static routers"))) resConf.push(line)
        });
        resConf.push(`interface ${dhcpInt.name}`);
        resConf.push(`static domain_name_servers=${dhcpInt.dnsIpAddress} 8.8.8.8`);
        resConf.push(`static ip_addres=${dhcpInt.ipAddress}`);
        resConf.push(`static routers=${dhcpInt.routerIpAddress}`);
        writeFileSync(DHCPCD_CONF_PATH, resConf.join('\n'));
        await this.systemctl("restart", "dhcpcd");
    }
    // return IP addresses or required interfaces (or all interfaces if none passes)
    // returns e.g. { "eth0": ["192.168.0.44"], "wlan0": ["192.168.45"]}
    getMyIPs(interfaces: string[] = []): Record<string, string[]> {
        const nets = networkInterfaces();
        const results: Record<string, string[]> = {};
        // initialize to empty if requested interfaces
        for (const intf of interfaces) results[intf] = [];
        for (const name of Object.keys(nets).filter(intf => interfaces.length == 0 || interfaces.includes(intf))) {
            for (const net of nets[name]||[]) {
                // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
                if (net.family === 'IPv4' && !net.internal) {
                    results[name].push(net.address);
                }
            }
        }
        return results;
    }
    getWpaSupplicantInfo(): WifiNetworkInfo[] {
        const wpaInfoS = readFileSync(WPA_SUPPLICANT_PATH, "utf-8").split('\n');
        const wpaInfo : WifiNetworkInfo[] = [];
        
        let insideNetwork = false;
        //ctrl_interface=/var/run/wpa_supplicant
        //update_config=1
        //country=US
        //
        //network={
        //        ssid="ssid"
        //        psk="password"
        //        key_mgmt=WPA-PSK
        //}
        
        var network: WifiNetworkInfo;
        wpaInfoS.forEach((line) => {
            line = line.trim();
            if (line.startsWith("network")) {
                insideNetwork=true;
                network = {
                    ssid:"?",
                    psk: "*",
                    keyMgmt: "WPA-PSK"
                };
            } else if (line.startsWith("}") && insideNetwork) {
                insideNetwork=false;
                wpaInfo.push(network);
            } else if (insideNetwork && line.startsWith('ssid="')) network.ssid = line.substring('ssid="'.length, -1);
            else if (insideNetwork && line.startsWith('psk="')) network.psk = line.substring('psk="'.length, -1);
            else if (insideNetwork && line.startsWith("key_mgmt=")) network.keyMgmt = line.substr("key_mgmt".length);
        });
        return wpaInfo;
    }
    addWpaSupplicantNetwork(wpaInfo: WifiNetworkInfo[]) {
        var wpaInfoS = "";
        wpaInfo.forEach((network) => wpaInfoS += 
`
network={
    ssid="${network.ssid}"
    psk="${network.psk}"
    key_mgmt=${network.keyMgmt}
}
`
        );
        writeFileSync(WPA_SUPPLICANT_PATH, wpaInfoS, { flag: 'a' });
    }
    async scanWifi(): Promise<WifiNetworkInfo[]> {
        const wifiScans: WifiNetworkInfo[] = [];
        try {
            const res = await this.runCmd("/usr/sbin/wpa_cli", ["-i", "wlan0", "scan"]);
            if (res!="OK") throw "wpa_cli didn't return OK";
            const scanres = await this.runCmd("/usr/sbin/wpa_cli", ["-i", "wlan0", "scan_results"]);
            scanres.split("\n").forEach((line) => {
                if (!line.startsWith("bssid")) { // headline
                    const splitted = line.split("\t");
                    const ssid = splitted[4];
                    console.debug("ssid: ", ssid);
                    // for some reason wpa_cli sometimes returns ssid like this 
                    // 58:d9:d5:9c:99:b2	5500	-36	[WPA-PSK-CCMP][WPA2-PSK+FT/PSK-CCMP][ESS]	\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00
                    if (ssid.charCodeAt(0) != 0 && !ssid.startsWith("\\x00") && 
                        // skip already seen ssids (several lines linked to frequencies we don't bother here)
                        !wifiScans.some(ws => ws.ssid === ssid)) {
                        // see flags format above. remove 1rst and lastt char and split on ][
                        const flags = splitted[3].slice(1, -1).split("][");
                        // console.debug("flags: ", flags);
                        const keyMgmt = flags.filter((flag) => flag.endsWith("-CCMP")).map((ccmp) => ccmp.slice(0,-5));
                        console.debug("key mgt: ", keyMgmt);
                        wifiScans.push({ssid, keyMgmt});
                    }
                }
            }); 

        } catch(err) {
            return Promise.reject("cannot scan wifi: " + JSON.stringify(err));
        }
        return wifiScans;
    }
}