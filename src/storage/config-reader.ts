import { PierServicesObject } from "../models/pier-service";
import { dump, load } from 'js-yaml';
import { readFileSync, writeFileSync } from "fs";

/*
 YAML file look like
 services:
  <service_name>:
    activated: true
    <other_param1>:<value1>
  <service_name2>:
    activated: true
    <other_param2>:<value2>
*/

// string: name of the service, IPierService: definition of the service
type SvcRecs = Record<'services', PierServicesObject>;

export class ConfigDBManager {
  constructor(private configPath: string) {}
  fetchData (): PierServicesObject {
		const yamlCfg: string = readFileSync(this.configPath, {encoding:'utf8'});
		console.log(`loaded config from ${this.configPath}: ${yamlCfg}`);
		const jsonCfg : SvcRecs= load(yamlCfg) as SvcRecs;
		console.log('Json parsed: ', jsonCfg);
		return jsonCfg.services ? jsonCfg.services : {};
	}

	persistData (svcs: PierServicesObject): void {
		const data: SvcRecs = { services: svcs };
		const yamlCfg: string = dump(data);
		console.log(`dumping config to ${this.configPath}: ${yamlCfg}`);
		writeFileSync(this.configPath, yamlCfg);
	}
}
