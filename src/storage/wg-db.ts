import { readFileSync, writeFileSync} from "fs";
import { WebSocketClient } from "./websocket-client";
import { WireguardPeer, WireguardPeerFull, WireguardPeerStr } from "../models/wireguard-peer";

const WgDir = "/etc/wireguard";
const CounterFile = WgDir + "/counter_file";
const PeerFragmentTemplate = WgDir + "/peer-fragment.conf";
const WG0 = WgDir + "/wg0.conf";
const PeerTemplate = WgDir + "/peer.conf";

export class WireguardDB extends WebSocketClient {
	getNewUserId(): number {
		const userId = parseInt(readFileSync(CounterFile, "utf8")) + 1;
		console.log("user id: ", userId); 
		if (userId > 255) {
			throw new Error("You have reached the maximum number of clients supported.\nTo add more clients manually, change the server configuration in /etc/wireguard/wg0.conf to a bigger subnet and manually configure your additional clients.");
		}
		writeFileSync(CounterFile, userId.toString());
		return userId;
	}
	getPeerFragment(userId: number, publicKey: string, allowedIPs: string | null = null): string {
		// get peer fragment template and replace placeholders with real values.
		const peerFragmentTemplate = readFileSync(PeerFragmentTemplate, "utf8");
		const res = peerFragmentTemplate.replace(/client_public_key/g, publicKey).replace(/user_id/g, userId.toString());
		return allowedIPs ? res.replace(/^AllowedIPs = .*$/m, "AllowedIPs = " + allowedIPs) : res;
	}
	createPeer(userId: number, clientPrivateKey: string, clientPublicKey: string): WireguardPeerStr {
		const peerFragment = this.getPeerFragment(userId, clientPublicKey);
		// append the fragment to wg0
		writeFileSync(WG0, peerFragment, { encoding: "utf8", flag: "a" });
		// get the client peer template and replace placeholders with real values.
		const peerTemplate = readFileSync(PeerTemplate, "utf8");
		const peer = peerTemplate.replace(/client_private_key/g, clientPrivateKey).replace(/user_id/g, userId.toString());
		// return the newly created peer
		return { id: userId, peer: peer };
	}
	private parseWG0() : { header: string, peers: WireguardPeerFull[] } {
		const lines = readFileSync(WG0, "utf8").split("\n").filter(line => !line.match(/^\s*$/));
		let header = "";
		const peers: WireguardPeerFull[] = [];
		let inPeer = false;
		let peerId = -1;
		let publicKey = "";
		let allowedIPs = "";
		lines.forEach(line => {
			//console.debug("wg0 line", line);
			if (line.match(/^\[Peer\]/)) {
				inPeer = true;
				//console.debug("in peer");
			} else if (inPeer) {
				//console.debug("looking peer line");
				const m = line.match(/^#Peer-([0-9]+)\s*/);
				if (m) {
					peerId = +m[1];
					//console.debug("peer id=", peerId);
				} else {
					const m = line.match(/^PublicKey\s*=\s*([^\s]+)\s*/);
					if (m) {
						publicKey = m[1];
						//console.debug("publick key=", publicKey);
					} else {
						const m = line.match(/^AllowedIPs\s*=\s*([^\s]+)\s*/);
						if (m) {
							allowedIPs = m[1];
							//console.debug("allowed ips=", allowedIPs);
							const one = { id: peerId, peer: { publicKey, allowedIPs } };
							peers.push(one);
							inPeer = false;
							//console.debug(JSON.stringify(one));
						}
					}
				}
			} else { !inPeer
				header += line + "\n";
			}
		});
		return { header, peers };

	}
	listPeers(): WireguardPeerFull[] {
		return this.parseWG0().peers;
	}
	removePeer(id: number): void {
		const wg = this.parseWG0();
		let content = wg.header;
		console.debug("removing peer", id);
		// rewrite all peers but the one with removed id.
		wg.peers.filter(peer => peer.id != id).forEach(peer => {
			content += this.getPeerFragment(peer.id, peer.peer.publicKey);
		})
		writeFileSync(WG0, content, { encoding: "utf8" });
	}
	replacePeer(id: number, peer: WireguardPeer): void {
		const wg = this.parseWG0();
		let content = wg.header;
		console.debug("replacing peer", id, "by", peer);
		wg.peers.forEach(item => {
			content += (item.id !== id) ?
				this.getPeerFragment(item.id, item.peer.publicKey, item.peer.allowedIPs) :
				this.getPeerFragment(id, peer.publicKey, peer.allowedIPs);
		});
		//console.log("replace by", content);
		writeFileSync(WG0, content, { encoding: "utf8" });
	}
	addExistingPeer(peer: WireguardPeer): number {
		const id = this.getNewUserId();
		const s = this.getPeerFragment(id, peer.publicKey, peer.allowedIPs);
		writeFileSync(WG0, s, { encoding: "utf8", flag: "a" });
		return id;
	}
}
