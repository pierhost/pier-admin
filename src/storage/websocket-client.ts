import { client as WSClient, connection as WSConnection, Message, IStringified } from "websocket";
import { Response} from 'express';
import { WEBSOCKET_URL, WEBSOCKET_JWT_KEY_FILE, WEBSOCKETD_LOG_FILE } from "../config";
import { readFileSync } from "fs";
import * as jwt from "jsonwebtoken";
import { EventEmitter } from "events";

export class CmdResult { status: number; out: string[] }
const NOT_FINISHED = NaN;

class WSPierClient { 
	client: WSClient = new WSClient();
	connection?: WSConnection;
	static asyncResults = new Map<number, CmdResult>();

	async connect(): Promise<WSConnection> {
		console.debug("Websocket connecting to", WEBSOCKET_URL, "...");
		this.client.connect(WEBSOCKET_URL);
		return new Promise((resolve, reject) =>{
		this.client.on('connectFailed', err => {
			console.error("websocket connection failed:", err);
			reject(err);
		})
		this.client.on('connect', (conn: WSConnection) => { 
			console.log('connected to websocket at', WEBSOCKET_URL, "state:", conn.state);
			conn.on('close', function(reasonCode: number, desc:string) {
				console.log('Websocket Connection Closed with status', reasonCode, 'and desc', desc);
			});
			conn.on('error', function(error) {
				console.log("Websocket Connection Error: " + error.toString());
			});
			resolve(conn);
		});
	});
	}
	async sendCmd(data: IStringified, asyncCmdRes?: AsyncCmdResult | Response): Promise<string> {
		this.connection = await this.connect();
		this.connection.sendUTF(data);
		if (asyncCmdRes instanceof AsyncCmdResult) {
			// create cache entry
			WSPierClient.asyncResults.set(asyncCmdRes.id, { status: NOT_FINISHED, out: []});
			// flush it after 5 mn
			setTimeout(() => this.clearCmdResult(asyncCmdRes.id), 5 * 60 * 1000);
		}
		return new Promise<string>((resolve, reject) => {
			this.connection?.addListener('message', data => {
				this.processMessage(data, resolve, reject, asyncCmdRes);
			});
		})
	}
	clearCmdResult(cmdId: number) {
		console.debug("clearing results for command #", cmdId);
		WSPierClient.asyncResults.delete(cmdId);
		this.connection?.connected && this.connection.close(WSConnection.CLOSE_REASON_NORMAL);
	}
	processMessage(data: Message, resolve: (value: string) => void, reject: (reason?: string) => void, asyncCmdRes?: AsyncCmdResult | Response) {
		if (data.type == 'binary') reject('Binary data not supported on websocket');
		else if (! data.utf8Data || data.utf8Data == '') reject('No data returned on websocket');
		else try {
			let reply: unknown;
			try {
				reply = JSON.parse(data.utf8Data);
			} catch (err) {
				console.debug("error when parsing websocket result", data.utf8Data);
				throw err;
			}
			const isCR = (reply as CmdResult).status != undefined && (reply as CmdResult).out != undefined;
			const asyncResult = asyncCmdRes instanceof AsyncCmdResult ? WSPierClient.asyncResults.get(asyncCmdRes.id) : undefined;
			if (typeof reply === "string") {
				console.debug('return one line: ', reply);
				if (asyncCmdRes)
					if (asyncCmdRes instanceof AsyncCmdResult) {
						if (asyncResult) asyncResult.out.push(reply);
						// else line is lost! Can happen if command last veeeery long after the result cache was flushed
						else console.error("lost output line", reply);
					} else asyncCmdRes.write(reply + "\n");
				else throw Error("don't know how to manage one line in sync mode");
			} else if (isCR) {
				const cr = reply as CmdResult;
				if (asyncCmdRes instanceof AsyncCmdResult) {
					if (asyncResult) {
						asyncResult.out.concat(cr.out);
						asyncResult.status = cr.status;
					}
					// for those who need to do something when an async command ends, trigger an event
					asyncCmdRes.emit(cr.status == 0 ? 'asyncDone' : 'asyncAborted', asyncCmdRes.id, asyncResult);
				} // if instanceof Response, will be managed in handler
				const result = cr.out.join('\n');
				console.log('Status: ', cr.status);
				console.log('Stdout: ', result);
				if (cr.status != 0) reject(result);
				else resolve(cr.out.join('\n'));
				this.connection?.close(WSConnection.CLOSE_REASON_NORMAL, "cmd done");
			} else {
				reject(`Unknown data: ${data.utf8Data}`);
				this.connection?.close(WSConnection.CLOSE_REASON_INVALID_DATA);
			}
		} catch (err) {
			console.error(err);
			reject(err);
			this.connection?.close(WSConnection.CLOSE_REASON_ABNORMAL);
		}
	}
}
export declare interface WebSocketClient {
    on(event: 'asyncDone', listener: (cmdId: number) => void): this;
    on(event: 'asyncAborted', listener: (cmdId: number) => void): this;
}
export class AsyncCmdResult extends EventEmitter {
	id: number;
	constructor() { 
		super();
		this.id = Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER + 1));
	}
}

const jwtKey = readFileSync(WEBSOCKET_JWT_KEY_FILE, "utf8");
export class WebSocketClient {
	private getToken() {
		// create a JWT token and stores it in a token cookie
		return jwt.sign({ payload: jwtKey }, jwtKey, {algorithm:"HS256", expiresIn: 120});
	}
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	async runCmd(cmd: string, args: string[], options?: Record<string, any>, stream?: Response): Promise<string> {
		console.debug(`Spawning cmd: ${cmd} ${args.join(" ")} ...`);
		if (stream) 
			if (options) options.sync = false; else options = { sync: false };
		const wsClient = new WSPierClient();
		return wsClient.sendCmd(JSON.stringify({ cmd: cmd, args: args, token: this.getToken(), ...options }), stream);
	}
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	asyncRunCmd(cmd: string, args: string[], options?: Record<string, any>): AsyncCmdResult {
		try {
			const asyncCmdRes = new AsyncCmdResult();
			if (options) options.sync = false; else options = { sync: false }
			console.debug(`Spawning async cmd #${cmd}: ${cmd} ${args.join(" ")} ...`);
			const wsClient = new WSPierClient();
			wsClient.sendCmd(JSON.stringify({ cmd: cmd, args: args, token: this.getToken(), ...options }), asyncCmdRes);
			return asyncCmdRes;
		} catch (err) {
			console.error(err);
			throw err;
		}
	}
	asyncGetResults(cmdId: number): CmdResult {
		console.debug(`Getting async cmd result: ${cmdId}`);
		const res = WSPierClient.asyncResults.get(cmdId);
		if (res) {
			const ret = { status: res.status, out: res.out };
			res.out = [];
			return ret;
		} else throw Error("NotFound");
	}
	async getWsdLogs(maxlines = 100): Promise<string> {
		return await this.runCmd("/usr/bin/tail", ["-" + maxlines, WEBSOCKETD_LOG_FILE]);
	}
    async signal (processName: string, signal: string | number): Promise<string> {
        return await this.runCmd("/usr/bin/killall", ["-"+signal, processName]);
    }
	async systemctl(command: "start"|"stop"|"restart"|"reload"|"status", service: string): Promise<string> {
        return await this.runCmd("/usr/bin/systemctl", [command, service]);
	}
}
