import { Router, Request, Response } from 'express';
import { LoginHandler } from "../handlers/login-handler";

const router = Router();
// first check we are logged in	
router.use(LoginHandler.validate)
  .options("*", (req:Request, res:Response) => { res.status(204).end(); });
// now declare all routes for authentication management
router
  .get("/auth/login", LoginHandler.isLogged)
  .post("/auth/login", LoginHandler.login)
  .get("/auth/password", LoginHandler.mustChangePassword)
  .post("/auth/password", LoginHandler.firstPassword)
  .put("/auth/password", LoginHandler.changePassword)
  .post("/auth/logout", LoginHandler.logout);
export default router;