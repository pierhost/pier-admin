import { Router } from 'express';
import { CronHandler } from "../handlers/cron-handler";

const router = Router();
router
  .get  ("/admin/crontab",          CronHandler.getCrontab)
  .post ("/admin/crontab",          CronHandler.addCronjob)
  .delete ("/admin/crontab",        CronHandler.removeCronjob)
  ;

export default router;