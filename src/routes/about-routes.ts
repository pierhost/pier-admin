import { Router } from 'express';
import { AboutHandler } from "../handlers/about-handler";

const router = Router();
router
  .get("/about", AboutHandler.about)

export default router;