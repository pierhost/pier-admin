import loginRouter from "./login-routes";
import adminRouter from "./admin-routes";
import wireguardRouter from "./wireguard-routes";
import aboutRouter from "./about-routes";
import dockerRouter from "./docker-routes";
import networkRouter from "./network-routes";
//import cronRouter from "./cron-routes";

export default [
    loginRouter,
    adminRouter,
    wireguardRouter,
    aboutRouter,
    dockerRouter,
    networkRouter,
//    cronRouter
]