import { Router } from 'express';
import { DockerHandler } from "../handlers/docker-handler";

const router = Router();
router
  .get("/containers", DockerHandler.listContainers)
  .post("/containers/:id/restart", DockerHandler.restartContainer)
  .get("/containers/:id", DockerHandler.getContainer)
  .get("/containers/:id/logs", DockerHandler.containerLogs)


export default router;