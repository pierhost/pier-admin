import { Router } from 'express';
import { WireGuardHandler } from "../handlers/wireguard-handler";

const router = Router();
router
  .post("/wireguard/peers", WireGuardHandler.createPeer)
  .get("/wireguard/peers", WireGuardHandler.listPeers)
  .delete("/wireguard/peers/:id", WireGuardHandler.removePeer)
  .put("/wireguard/peers/:id", WireGuardHandler.updatePeer)
  .put("/wireguard/peers", WireGuardHandler.addExistingPeer);

export default router;