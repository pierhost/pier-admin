import { Router } from 'express';
import { NetWorkHandler } from "../handlers/network-handler";

const router = Router();
router
  .get ("/admin/network/domain-and-contact", NetWorkHandler.getContactAndDomain)
  .put ("/admin/network/domain-and-contact", NetWorkHandler.updateContactAndDomain)
  .get ("/admin/network/interfaces/default", NetWorkHandler.getDefaultNetworkInterface)
  .put ("/admin/network/interfaces/default", NetWorkHandler.changeNetworkInterface)
  .get ("/admin/network/interfaces", NetWorkHandler.getInterfacesIp)
  .get ("/admin/network/ethernet/connected", NetWorkHandler.ethernetConnected)
  .get ("/admin/network/wifi", NetWorkHandler.scanWifi)
  .post("/admin/network/wifi", NetWorkHandler.addWifiNetwork)
  ;

export default router;