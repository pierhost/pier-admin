import { Router } from 'express';
import { AdminHandler } from "../handlers/admin-handler";

const router = Router();
router
  .get  ("/admin/ping",             AdminHandler.ping)
  .get  ("/admin/config/admin",     AdminHandler.getAdminConfig)
  .post ("/admin/config/admin",     AdminHandler.updateAdminConfig)
  .get  ("/admin/config/critical",  AdminHandler.getCriticalConfig)
  .post ("/admin/config/critical",  AdminHandler.updateCriticalConfig)
  .get  ("/admin/config/user",      AdminHandler.getUserConfig)
  .post ("/admin/config/user",      AdminHandler.updateUserConfig)
  .post ("/admin/update/admin",     AdminHandler.streamUpdateAdmin)
  .post ("/admin/update/critical",  AdminHandler.streamUpdateCritical)
  .post ("/admin/update/user",      AdminHandler.streamUpdateUser)
  .post ("/admin/update/all",       AdminHandler.streamUpdateAll)
  .post ("/admin/system/update",    AdminHandler.streamUpdateSystem)
  .post ("/admin/system/reboot",    AdminHandler.streamRebootServer)
  .get  ("/admin/passwords",        AdminHandler.getAllPasswords)
  .get  ("/admin/wsdlogs",          AdminHandler.getWsdLogs)
  .get  ("/admin/cmdresult/:id",    AdminHandler.getCmdResults)
  ;

export default router;