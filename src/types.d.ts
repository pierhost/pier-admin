declare namespace Express {
	export interface Request {
		auth?: boolean;
	}
	export interface Response {
		token: unknown;
	}
}