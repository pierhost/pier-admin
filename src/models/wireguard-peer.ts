export interface WireguardPeer { publicKey: string, allowedIPs: string }
export interface WireguardPeerFull { id: number, peer: WireguardPeer }
export interface WireguardPeerStr { id: number, peer: string }
