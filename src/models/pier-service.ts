export interface IPierService {
	activated: boolean;
	// only for traefik
	challenge?: 'tls'|'http';
	https?: boolean;	
}
export type PierServicesObject = Record<string, IPierService>;
