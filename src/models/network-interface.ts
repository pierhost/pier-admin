export interface DhcpNetworkInterface {
    name: string; // "eth0" | "wlan0";
    ssid?: string; // for wifi only
    ipAddress: string;
    routerIpAddress: string;
    dnsIpAddress: string;
}

export interface WifiNetworkInfo {
    ssid:string;
    psk?:string;
    keyMgmt: string | string[]; // eg WPA-PSK
}
