//###################### DIRECTORIES ##############################
// change to ./test/resources for testing or ln -s ./test/resources /home/pier
export const PIER_HOME = process.env.PIER_HOME || "/home/pier";
// WARNING SERVICES_DIR is for the websocketd here. 
export const SERVICES_DIR = process.env.SERVICES_DIR || PIER_HOME + "/Services";
// Internally to the docker pier-admin, it is mounted on /services
// *** FOR LOCAL TESTING, YOU MUST SET THE ISERVICES_DIR TO THE SAME VALUE AS SERVICES_DIR ***
export const ISERVICES_DIR = process.env.ISERVICES_DIR || "/services"
export const ADMIN_DIR = process.env.ADMIN_DIR || PIER_HOME + "/Admin";
export const SECRETS_DIR = process.env.SECRETS_DIR || SERVICES_DIR + "/Secrets";
export const ISECRETS_DIR = process.env.ISECRETS_DIR || ISERVICES_DIR + "/Secrets";
export const GEN_DIR = process.env.GEN_DIR || SERVICES_DIR + "/Generated";
export const IGEN_DIR = process.env.IGEN_DIR || ISERVICES_DIR + "/Generated";
export const HOTSPOT_DIR = process.env.HOTSPOT_DIR || "/hotspot";
//###################### CONFIG VARS ##############################
export const APP_PORT = process.env.APP_PORT || 4000;
export const JWT_KEY = process.env.JWT_KEY || "put-your-secret-here";  // default value for test only
export const JWT_TIMEOUT = process.env.JWT_TIMEOUT ? parseInt(process.env.JWT_TIMEOUT) : 60; // minutes
export const BASE_URL = process.env.BASE_URL || '/api';
export const WEBSOCKET_URL = process.env.WEBSOCKET_URL || 'ws://localhost:8088';  // default value for test only
export const WEBSOCKET_JWT_KEY_FILE = process.env.WEBSOCKET_JWT_KEY_FILE || ISECRETS_DIR + '/WEBSOCKETD_KEY';
export const SERVICES_DEFAULT_BRANCH = process.env.SERVICES_DEFAULT_BRANCH || "develop";

//###################### CONFIG FILES ##############################
// All files in ISERVICES_DIR are mounted through the /services dir in the container
export const CONFIG_PATH = process.env.CONFIG_PATH || ISERVICES_DIR + "/config.yml";
export const ADMIN_CONFIG_PATH = process.env.ADMIN_CONFIG_PATH || ISERVICES_DIR + "/pier-services/admin_config.yml";
export const CRITICAL_CONFIG_PATH = process.env.CRITICAL_CONFIG_PATH || ISERVICES_DIR + "/pier-services/critical_config.yml";
export const SAMPLE_CONFIG_PATH = process.env.SAMPLE_CONFIG_PATH || ISERVICES_DIR + "/pier-services/sample_config.yml";
export const ENV_PATH = process.env.ENV_PATH || ISERVICES_DIR + "/.env"; 
export const LOGIN_PATH = process.env.LOGIN_PATH || ISECRETS_DIR + "/pwd";
// Default value assumes that /etc/dhcpcd.conf and /etc/wpa_supplicant/wpa_supplicant.conf from host are mounted
// at the "same place" in the container. This should'nt harm as these files are not used by alpine usually
export const DHCPCD_CONF_PATH = process.env.DHCPCD_CONF_PATH || "/etc/dhcpcd.conf";
// Default value assumes that /etc/wpa_supplicant/wpa_supplicant.conf from host is mounted on /etc/dhcpcd.conf in container. 
export const WPA_SUPPLICANT_PATH = process.env.WPA_SUPPLICANT_PATH || "/etc/wpa_supplicant/wpa_supplicant.conf"
//###################### COMMANDS ##############################
// These commands will be run by the websocketd service, so use real SERVICES_DIR or real ADMIN_DIR, not volume one
export const REGENSRV_CMD = process.env.REGENSRV_CMD || SERVICES_DIR + '/regenerate-services.sh';
export const UPDATE_SYSTEM_CMD = process.env.UPDATE_SYSTEM_CMD || ADMIN_DIR + '/update-system.sh';
export const WEBSOCKETD_LOG_FILE = process.env.WEBSOCKETD_LOG_FILE ||  "/var/log/websocketd.log";
export const HOTSPOT_CMD = process.env.HOTSPOT_CMD || HOTSPOT_DIR + 'hotspot.sh';
// semicolon separated list of allowed domains. Normally set in ecosystem.config.ts
// default value can be used only in test mode. CORS_DOMAINS not needed in production modeas we use the same domain
export const CORS_DOMAINS = process.env.CORS_DOMAINS || 'http://localhost:9090;http://localhost:8080'; 

export const TIMEOUT_ERROR = -100;