
// to be called with await sleep(x)
export function sleep(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
}
