import { Request, Response} from 'express';
import { HandlerUtils } from './handler-utils';
import { AdminEntryPoint, confKind } from '../entrypoints/admin-entrypoint';
import { TIMEOUT_ERROR } from '../config';
import { PierServicesObject } from '../models/pier-service';
import { CronJob } from '../models/cronjob';
export class AdminHandler {
    public static ping(request: Request, response: Response): void {
		new HandlerUtils(request, response).respond(200, { result: "pong" });
    }
    public static async streamUpdateServices(request: Request, response: Response, kind: confKind): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await AdminEntryPoint.streamUpdateServices(kind, response);
            handlerUtils.respond(200, res);
        } catch(e) {
            handlerUtils.respond(500, `Update ${kind} services failed, see logs on server`);
        }
    }
    public static async streamUpdateAdmin(request: Request, response: Response): Promise<void> {
        return await AdminHandler.streamUpdateServices(request, response, "admin");
    }
    public static async streamUpdateCritical(request: Request, response: Response): Promise<void> {
        return await AdminHandler.streamUpdateServices(request, response, "critical");
    }
    public static async streamUpdateUser(request: Request, response: Response): Promise<void> {
        return await AdminHandler.streamUpdateServices(request, response, "user");
    }
    public static async streamUpdateAll(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            // important, admin must be restarted last as it breaks the code...
            await AdminEntryPoint.streamUpdateServices("user", response);
            await AdminEntryPoint.streamUpdateServices("critical", response);
            await AdminEntryPoint.streamUpdateServices("admin", response);
            handlerUtils.respond(200, { result: "full restart ok" });
        } catch(e) {
            handlerUtils.respond(500, `Update all failed, see logs on server`);
        }
	}
    public static async streamUpdateSystem(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await AdminEntryPoint.streamUpdateSystem(response);
            handlerUtils.respond(200, res);
        } catch(e) {
            if (e.status == TIMEOUT_ERROR)
                handlerUtils.respond(408, "OS Update timed out, see logs on server");
            else
                handlerUtils.respond(500, "Operating System Update failed, see logs on server");
        }
    }
    public static async streamRebootServer(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await AdminEntryPoint.streamRebootServer(response);
            handlerUtils.respond(200, res);
        } catch(e) {
            handlerUtils.respond(500, "Reboot failed, see logs on server");
        }
    }
    public static getAllPasswords(request: Request, response: Response): void {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const pwds = AdminEntryPoint.getAllPasswords();
            handlerUtils.respond(200, pwds);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, "get all passwords failed, see logs on server");
        }
    }
    public static async getConfig(request: Request, response: Response, kind: confKind): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        const checkNew = (kind === "user" && request.query['check-new']);
        try {
            console.debug("request params", request.params);
            const config = checkNew ?
                await AdminEntryPoint.getDeltaUserConfig() :
                AdminEntryPoint.getConfig(kind);
            handlerUtils.respond(200, config);
        } catch(e) {
            console.error("get config error:", e);
            const kd = checkNew ? "new services" : kind; 
            handlerUtils.respond(500, `get ${kd} config failed, see logs on server`);
        }
    }
    public static async getAdminConfig(request: Request, response: Response): Promise<void> {
        return await AdminHandler.getConfig(request, response, "admin");
    }
    public static async getCriticalConfig(request: Request, response: Response):  Promise<void> {
        return await AdminHandler.getConfig(request, response, "critical");
    }
    public static async getUserConfig(request: Request, response: Response):  Promise<void> {
        return await AdminHandler.getConfig(request, response, "user");
    }
    public static async updateConfig(request: Request, response: Response, kind: confKind): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		try {
			handlerUtils.checkBody();
			const lst = request.body as PierServicesObject;
			try {
				const res = await AdminEntryPoint.putConfig(lst, kind);
				handlerUtils.respond(200, { result: res });
			} catch(e) {
				if (e.status == TIMEOUT_ERROR) handlerUtils.respond(408, "Update services time out");
                else handlerUtils.respond(500, "Update services failed, see logs on server");
			}
		} catch(e) {
			handlerUtils.checkResponse(e, 400, "Invalid services list");
		}
	}
    public static async updateAdminConfig(request: Request, response: Response): Promise<void> {
        return await AdminHandler.updateConfig(request, response, "admin");
    }
    public static async updateCriticalConfig(request: Request, response: Response): Promise<void> {
        return await AdminHandler.updateConfig(request, response, "critical");
    }
    public static async updateUserConfig(request: Request, response: Response): Promise<void> {
        return await AdminHandler.updateConfig(request, response, "user");
    }
// async methods not used anymore
    public static asyncUpdateServices(request: Request, response: Response, kind: confKind): void {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            handlerUtils.asyncRespond(AdminEntryPoint.asyncUpdateServices(kind).id);
        } catch(e) {
            handlerUtils.respond(500, `Update ${kind} services failed, see logs on server`);
        }
    }
    public static asyncUpdateAdmin(request: Request, response: Response): void {
        AdminHandler.asyncUpdateServices(request, response, "admin");
    }
    public static asyncUpdateCritical(request: Request, response: Response): void {
        AdminHandler.asyncUpdateServices(request, response, "critical");
    }
    public static asyncUpdateUser(request: Request, response: Response): void {
        AdminHandler.asyncUpdateServices(request, response, "user");
    }
// cronjob management, not yet exposed
    public static async manageCronjob(request: Request, response: Response, remove: boolean): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const params = request.body as CronJob;
            if (!remove && !params.every) handlerUtils.respond(400, "Cronjob 'every' parameter missing");
            else {
                await AdminEntryPoint.manageCronjob(params.command, params.args, params.every, remove);
                handlerUtils.respond(200, "Crontab changed");
            }
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, "Cronjob adding/removing failed, see logs on server");
        }
    }
// logs mgt
    public static async getWsdLogs(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await AdminEntryPoint.getWsdLogs();
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, "Failed to get websocketd logs, see logs on server");
        }
    }
    public static getCmdResults(request: Request, response: Response): void {
		const handlerUtils = new HandlerUtils(request, response);
        if (!request.params || !request.params.id) {
			handlerUtils.error(400, 'Invalid id');
		}
		const id = +request.params.id;
        try {
            const res = AdminEntryPoint.getCmdResults(id);
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(404, e.message);
        }
    }
}