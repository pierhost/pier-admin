import { Request, Response } from 'express';
import * as jwt from "jsonwebtoken";
import { LoginDB } from "../storage/login-db";
import { JWT_KEY, JWT_TIMEOUT } from "../config";
import { NextFunction } from 'express';
import { HandlerUtils } from './handler-utils';

export class LoginHandler {
	static jwtSign(response: Response): void {
		// create a JWT token and stores it in a token cookie
		const token = jwt.sign({}, JWT_KEY, {algorithm:"HS256", expiresIn:JWT_TIMEOUT+'m'});
		response.cookie("token", token, { sameSite: "lax", httpOnly: true} );
	}
	static validate (request: Request, response: Response, next: NextFunction): void {
		// log requests
		console.info(">received request", request.method, request.url);
		const protectedPaths = [ "/auth/password", "/auth/changePassword" ];
		console.debug(">headers", request.headers, "\n>body", protectedPaths.includes(request.url) ? "{ *** sensitive content masked *** }" : request.body);

		const token = request.cookies.token;
		try {
			/*const payload =*/ jwt.verify(token, JWT_KEY);
			request.auth = true;
			// extends token expiration.
			LoginHandler.jwtSign(response);
		} catch (e) {
			request.auth = false;
		}
		// no check if :
		if (
			request.auth || // already logged
			request.method == 'OPTIONS' ||  // OPTIONS request should not be checked
			request.path == '/auth/login' || // trying to login
			request.path == '/auth/logout' || // trying to login
			(request.path == '/auth/password' && request.method == 'GET')  // checking if password must be changed

		)
			return next();
		else LoginDB
			.mustChangePwd()
			.then(mustChange => {
				if (mustChange) {
					if (request.path == ('/auth/password') && request.method == 'POST') {
						return next();
					} else {
						new HandlerUtils(request, response).respond(401, "You must set a password first");
					}
				} else {
					new HandlerUtils(request, response).respond(401, "Not logged in");
				}
			});
}
	static async mustChangePassword (request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		try {
			const mustChange = await LoginDB.mustChangePwd();
			handlerUtils.respond(200, { mustChange: mustChange });
		} catch (e) {
			handlerUtils.respond(500, "Internal error");
		}
	}
	static async firstPassword (request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		const body = request.body;
		try {
			LoginDB.firstPwd(body.password);
			handlerUtils.respond(200, "Password changed");
		} catch (e) {
			handlerUtils.respond(403, e.message);
		}
	}
	static async changePassword (request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		const body = request.body;
		//console.debug(body);
		if (!body || !body.oldPassword || !body.newPassword)
			handlerUtils.respond(400, "Bad data on changing password");
		else
			try {
				LoginDB.changePwd(body.oldPassword, body.newPassword);
				handlerUtils.respond(200, "Password changed");
			} catch (e) {
				console.log("change pwd error", e);
				handlerUtils.respond(403, e.message);
			}
	}
	static async login (request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		const mustChange = await LoginDB.mustChangePwd()
		if (mustChange) {
			handlerUtils.respond(403, "Default password must be changed first");
		} else if (request.body.password) {
			if (LoginDB.checkPwd(request.body.password)) {
				LoginHandler.jwtSign(response);
				handlerUtils.respond(200, 'Logged');
			} else {
				handlerUtils.respond(403, "Wrong password");
			}
		} else {
			handlerUtils.respond(400, "Password not set");
		}
	}
	static async logout (request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		response.cookie("token", "");
		handlerUtils.respond(200, "Logged Out");
	}
	static async isLogged (request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		if (request.auth) handlerUtils.respond(200, "Logged");
		else handlerUtils.respond(401, "Not logged");
	}
}