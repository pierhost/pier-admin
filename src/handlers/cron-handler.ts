import { Request, Response} from 'express';
import { HandlerUtils } from './handler-utils';
import { CronEntryPoint } from '../entrypoints/cron-entrypoint';
import { CronJob } from '../models/cronjob';
export class CronHandler {
    public static async manageCronjob(request: Request, response: Response, remove: boolean): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const params = request.body as CronJob;
            if (!remove && !params.every) handlerUtils.respond(400, "Cronjob 'every' parameter missing");
            else {
                await CronEntryPoint.manageCronjob(params.command, params.args, params.every, remove);
                handlerUtils.respond(200, "Crontab changed");
            }
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, "Cronjob adding/removing failed, see logs on server");
        }
    }
    public static async addCronjob(request: Request, response: Response): Promise<void> {
        return await CronHandler.manageCronjob(request, response, false);
    }
    public static async removeCronjob(request: Request, response: Response): Promise<void> {
        return await CronHandler.manageCronjob(request, response, true);
    }
    public static async getCrontab(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await CronEntryPoint.getCrontab();
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, "Failed to get crontab, see logs on server");
        }
    }
}