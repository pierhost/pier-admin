import {Request, Response, NextFunction} from 'express';
import { CORS_DOMAINS } from '../config';

export class StopHandler extends Error {
	status: number;
	constructor(status: number) {
		super();
		this.status=status;
	}
}
// safely handles circular references
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function JSON_safeStringify (obj: any, indent = 2): string {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let cache: any[] = [];
	const retVal = JSON.stringify(
		obj,
		(key, value) =>
		typeof value === "object" && value !== null
			? cache.includes(value)
			? undefined // Duplicate reference found, discard key
			: cache.push(value) && value // Store value in our collection
			: value,
		indent
	);
	cache = [];
	return retVal;
  }

export class HandlerUtils<T=unknown> {
	constructor(private request: Request, private response: Response) {}
	// manage Headers & CORS
	static	manageHeaders(req: Request, res: Response, next: NextFunction): void {
		res.setHeader('Content-Type', 'application/json; charset=utf-8');
		// check the semi column separated list of allowed domains
		const allowedOrigins = CORS_DOMAINS.split(";").filter(val => (val!=""));
		const origin = req.headers.origin as string;
		if (allowedOrigins.includes(origin)) {
			res.setHeader('Access-Control-Allow-Origin', origin);
		}
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin");
		res.header("Access-Control-Allow-Credentials", "true");
		res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		res.header("Access-Control-Max-Age", "3600"); // don't send options within 1 hour
		next();
	}
	respond(status: number, msg: unknown): void {
		if (typeof msg == 'string') msg = { msg: msg };
		const answer = JSON_safeStringify(msg);
		console.info(`response status=${status}, ${answer}`);
		if (this.response.headersSent) {
			this.response.status(status).write(answer + '\n');
			this.response.end();
		} else this.response.status(status).send(answer);
		
	}
	asyncRespond(rspId: number): void {
		this.respond(200, { followUpOn: rspId });
	}
	error(status: number, msg: string): void {
		this.respond(status, msg);
		throw new StopHandler(status);
	}
	checkResponse(e: unknown, status: number, msg: unknown): void {
		if (!(e instanceof StopHandler)) this.respond(status, msg);
		// if StopHandler, already done, see error method
	}
	checkBody(): void {
		if (!this.request.body)
			this.error (400, "Invalid user data");
	}
	checkName(): string {
		if (!this.request.params || !this.request.params.name) {
			this.error(400, 'Invalid name');
		}
		return this.request.params.name;
	}
	cast(): T {
		this.checkBody();
		try {
			return this.request.body as T;
		} catch(e) {
			this.error(406, "Incorrect user data. Should be of type ${T}");
			throw Error("useless"); // just for removing the error on no return.
		}
	}
}
