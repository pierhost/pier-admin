import { Request, Response} from 'express';
import { HandlerUtils } from './handler-utils';
import pack from "../../package.json";
import axios from "axios";

const pierAdminUiGitlabProjectId = "23205669";
const pierAdminApiGitlabProjectId = "22314059";
const gitlabReleaseUrl = (projectId: string) : string =>  `https://gitlab.com/api/v4/projects/${projectId}/releases`;

export class AboutHandler {
    public static async about(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res : Record<string, unknown> = {
                package: pack.name,
                version: pack.version,
            };
            const uiReleases = await axios.get(gitlabReleaseUrl(pierAdminUiGitlabProjectId));
            if (uiReleases.data.length > 1) res["ui-latest-release"] = uiReleases.data[0]["tag_name"];
            const apiReleases = await axios.get(gitlabReleaseUrl(pierAdminApiGitlabProjectId));
            if (apiReleases.data.length > 1) res["api-latest-release"] = apiReleases.data[0]["tag_name"];
            handlerUtils.respond(200, res);
        } catch (err) {
            handlerUtils.error(500, err.message);
        }
    }
}