import { Request, Response} from 'express';
import { HandlerUtils } from './handler-utils';
import { dockerEntryPoint, PierContainer } from '../entrypoints/docker-entrypoint';
export class DockerHandler {
	static async listContainers(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		const containers = await dockerEntryPoint.listContainers();
        handlerUtils.respond(200, containers);
	}
    static getContainer(request: Request, response: Response): PierContainer {
		const handlerUtils = new HandlerUtils(request, response);
        if (!request.params || !request.params.id) {
            handlerUtils.error(400, 'Invalid id');
        }
        const contId = request.params.id;
        try {
            const container = dockerEntryPoint.getContainer(contId);
            if (!container) handlerUtils.error(404, '{msg: "container not found"}');
            return container;
        } catch (err) {
            handlerUtils.error(404, '{msg: "container not found"}');
            throw Error; // never reached
        }
    }
	static async restartContainer(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		const pcontainer = DockerHandler.getContainer(request, response);
        pcontainer.container.restart();
        handlerUtils.respond(200, "done");
	}
	static async containerLogs(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		const logs = await (DockerHandler.getContainer(request, response).getLogs());
        handlerUtils.respond(200, logs);
	}
}
