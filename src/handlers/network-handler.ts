import { Request, Response} from 'express';
import { HandlerUtils } from './handler-utils';
import { NetworkEntryPoint, DomainAndContact } from '../entrypoints/network-entrypoint';
import { DhcpNetworkInterface, WifiNetworkInfo } from "../models/network-interface";
export class NetWorkHandler {
    public static getContactAndDomain(request: Request, response: Response): void {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const dnc = NetworkEntryPoint.getDomainAndContact();
            handlerUtils.respond(200, dnc);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, "get Domain & Contact failed, see logs on server");
        }
    }
    public static async updateContactAndDomain(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const dnc = handlerUtils.cast() as DomainAndContact;
            await NetworkEntryPoint.updateDomainContact(dnc);
            handlerUtils.respond(200, { msg: "Domain & Contact Updated" });
        } catch(e) {
            handlerUtils.respond(500, "Update Domain & Contact failed, see logs on server");
        }
    }
    public static changeNetworkInterface(request: Request, response: Response): void {
		const handlerUtils = new HandlerUtils(request, response);
        const pars = request.params;
        if (!pars.interface ||
            !pars.dns_ip_address ||
            !pars.ip_address ||
            !pars.router_ip_address) {
			handlerUtils.error(400, 'Invalid parameters');
		}
		const dhcpInt: DhcpNetworkInterface = { name: pars.interface, ipAddress: pars.ip_addres, dnsIpAddress: pars.dns_ip_address, routerIpAddress: pars.router_ip_address};
        try {
            const res = NetworkEntryPoint.changeNetworkInterface(dhcpInt);
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(404, e.message);
        }
    }
    public static async getDefaultNetworkInterface(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await NetworkEntryPoint.getDefaultNetworkInterface();
            console.debug("ip route", res);
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(404, e.message);
        }
    }
	public static  getInterfacesIp(request: Request, response: Response): void {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = NetworkEntryPoint.getInterfacesIp();
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(404, e.message);
        }
    }
	public static async ethernetConnected(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await NetworkEntryPoint.ethernetConnected();
            console.debug("eth is connected:", res);
            handlerUtils.respond(200, { "ethernetConnected" : res});
        } catch(e) {
            console.error(e);
            handlerUtils.respond(500, e.message);
        }
    }
    public static async  scanWifi(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        try {
            const res = await NetworkEntryPoint.scanWifi();
            handlerUtils.respond(200, res);
        } catch(e) {
			console.error(e);
            handlerUtils.respond(404, e.message || e);
        }
    }
    public static async addWifiNetwork(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
        const wifiNetworkInfo = request.body as WifiNetworkInfo;
        if (!wifiNetworkInfo.ssid ||
            !wifiNetworkInfo.keyMgmt ||
            !wifiNetworkInfo.psk) {
			handlerUtils.error(400, 'Invalid parameters ' + JSON.stringify(wifiNetworkInfo));
            return;
		}
        try {
            const res = await NetworkEntryPoint.addWifiNetwork(wifiNetworkInfo, response);
            handlerUtils.respond(200, res);
        } catch(e) {
            console.error(e);
            handlerUtils.respond(404, e.message);
        }
    }
}