import { WireGuardEntryPoint } from '../entrypoints/wireguard-entrypoint';
import { Request, Response} from 'express';
import { HandlerUtils, StopHandler } from './handler-utils';
import { WireguardPeer } from "../models/wireguard-peer";
import { TIMEOUT_ERROR } from '../config';

export class WireGuardHandler {
	public static async createPeer(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		try {
			const peer = await WireGuardEntryPoint.createPeer();
			handlerUtils.respond(200, peer);
		} catch(e) {	
			console.error(e.message);
			if (!(e instanceof StopHandler)) 
				if (e.status == TIMEOUT_ERROR)
					handlerUtils.respond(408, "Creation peer time out");
				else
					handlerUtils.respond(500, "Error when creating new Wireguard Peer , see server logs");
		}
	}
	public static listPeers(request: Request, response: Response): void {
		const handlerUtils = new HandlerUtils(request, response);
		try {
			handlerUtils.respond(200, WireGuardEntryPoint.listPeers());
		} catch(e) {	
			console.error(e.message);
			if (!(e instanceof StopHandler)) 
				if (e.status == TIMEOUT_ERROR)
					handlerUtils.respond(408, "List peers time out");
				else
					handlerUtils.respond(500, "Error when listing Wireguard Peers, see server logs");
		}
	}
	public static async removePeer(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils(request, response);
		if (!request.params || !request.params.id) {
			handlerUtils.error(400, 'Invalid id');
		}
		const id = +request.params.id;
		try {
			await WireGuardEntryPoint.removePeer(id); 
			handlerUtils.respond(200, "peer deleted");
		} catch(e) {	
			console.error(e.message);
			if (!(e instanceof StopHandler)) 
				if (e.status == TIMEOUT_ERROR)
					handlerUtils.respond(408, "Deleting peer time out");
				else
					handlerUtils.respond(500, "Error when deleting Wireguard Peer, see server logs");
		}
	}
	public static async updatePeer(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils<WireguardPeer>(request, response);
		if (!request.params || !request.params.id) {
			handlerUtils.error(400, 'Invalid peer id in request');
		}
		const id = +request.params.id;
		try {
			const peer : WireguardPeer = handlerUtils.cast();
			await WireGuardEntryPoint.updatePeer(id, peer);
			handlerUtils.respond(200, "peer replaced");
		} catch(e) {	
			console.error(e.message);
			if (!(e instanceof StopHandler)) 
				if (e.status == TIMEOUT_ERROR)
					handlerUtils.respond(408, "Replacing peer time out");
				else
					handlerUtils.respond(500, "Error when replacing peer, see server logs");
		}
	}
	public static async addExistingPeer(request: Request, response: Response): Promise<void> {
		const handlerUtils = new HandlerUtils<WireguardPeer>(request, response);
		try {
			const peer : WireguardPeer = handlerUtils.cast();
			const id = await WireGuardEntryPoint.addExistingPeer(peer)
			handlerUtils.respond(200, { id });
		} catch(e) {
			console.error(e.message);
			if (!(e instanceof StopHandler)) 
				if (e.status == TIMEOUT_ERROR)
					handlerUtils.respond(408, "Adding peer time out");
				else
					handlerUtils.respond(500, "Error when adding Wireguard Peer, see server logs");
		}
	}
}