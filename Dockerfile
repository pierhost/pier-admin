FROM node:lts-alpine@sha256:b8d48b515e3049d4b7e9ced6cedbe223c3bc4a3d0fd02332448f3cdb000faee1 as build-stage
ARG TARGETARCH
WORKDIR /pier-admin

# install all node modules (needed for tsc)
# python, make and gcc installed only for node-gyp on arm :/
COPY package.json tsconfig.json ./
RUN apk add python3 make build-base && npm install  --target_arch=$TARGETARCH

COPY ./src ./src
RUN npm run build  --target_arch=$TARGETARCH
# prune non production node modules
RUN npm prune --production
# clean unneeded directories
RUN rm -rf ./src ./test 

# restart a clean final stage
FROM node:lts-alpine@sha256:b8d48b515e3049d4b7e9ced6cedbe223c3bc4a3d0fd02332448f3cdb000faee1 as final-stage
WORKDIR /pier
COPY --from=build-stage /pier-admin/node_modules /pier/node_modules/
COPY --from=build-stage /pier-admin/bin /pier/

VOLUME ["/wireguard","/services", "/config"]

EXPOSE 4000

CMD [ "/usr/local/bin/node", "/pier/src/main.js" ]
